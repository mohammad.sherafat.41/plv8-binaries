# plv8-binaries
All binaries compiled in Ubuntu 20.04 with ICU enabled!

> *lsb_release -a*
```ini
No LSB modules are available.
Distributor ID:	Ubuntu
Description:	Ubuntu 20.04.3 LTS
Release:	20.04
Codename:	focal
```


## Requirement
> sudo apt install p7zip-full libc++-dev libc++abi-dev

## Postgresql 12.9
```bash
7z x plv8-3.0.0-bin-pg-12.9.7z
tar -xf plv8-3.0.0-bin-pg-12.9.tar -C /
cd '/usr/lib/postgresql/12/lib/bitcode' && /usr/lib/llvm-10/bin/llvm-lto -thinlto -thinlto-action=thinlink -o plv8-3.0.0.index.bc plv8-3.0.0/plv8.bc plv8-3.0.0/plv8_type.bc plv8-3.0.0/plv8_func.bc plv8-3.0.0/plv8_param.bc plv8-3.0.0/plv8_allocator.bc plv8-3.0.0/coffee-script.bc plv8-3.0.0/livescript.bc
```


## Postgresql 14.1
```bash
7z x plv8-3.0.0-bin-pg-14.1.7z
tar -xf plv8-3.0.0-bin-pg-14.1.tar -C /
cd '/usr/lib/postgresql/14/lib/bitcode' && /usr/lib/llvm-10/bin/llvm-lto -thinlto -thinlto-action=thinlink -o plv8-3.0.0.index.bc plv8-3.0.0/plv8.bc plv8-3.0.0/plv8_type.bc plv8-3.0.0/plv8_func.bc plv8-3.0.0/plv8_param.bc plv8-3.0.0/plv8_allocator.bc plv8-3.0.0/coffee-script.bc plv8-3.0.0/livescript.bc
```
